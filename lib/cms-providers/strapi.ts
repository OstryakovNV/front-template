import { STRAPI_URL } from '@lib/constants';

export function getStrapiURL(path = "") {
  return `${STRAPI_URL}${path}`;
}

// Helper to make GET requests to Strapi
export async function fetchAPI(path, init = undefined) {
  const requestUrl = `${STRAPI_URL}${path}`;
  const response = await fetch(requestUrl, init);
  const data = await response.json();
  return data;
}