import styled, { css } from 'styled-components'

interface Hamburger {
  isToggled?: boolean,
  handleChange?: any,
}

const Hamburger = ({ isToggled, handleChange }: Hamburger) => {
  return (
    <HamburgerWrapper onClick={handleChange}>
      <HamburgerBox>
        <HamburgerInner isToggled={isToggled} />
      </HamburgerBox>
    </HamburgerWrapper>
  )
}

const HamburgerWrapper = styled('div')`
  margin: 0;
  cursor: pointer;
  transition-timing-function: linear;
  transition-duration: .15s;
  transition-property: opacity,filter;
  text-transform: none;
  color: inherit;
  border: 0;
  background-color: transparent;
  margin-left: auto;
  background: ${p => p.theme.colors.purple};
  border-radius: 50%;
  width: 40px;
  height: 40px;
`

const HamburgerBox = styled('div')`
  position: relative;
  display: flex;
  width: 2.22rem;
  height: 100%;
  width: 100%;
  justify-content: center;
  align-items: center;
`

const HamburgerInner = styled('div') <Hamburger>`
  position: absolute;
  width: 1.45rem;
  height: 3px;
  transition-timing-function: ease;
  transition-duration: .15s;
  transition-property: transform;
  border-radius: 4px;
  background-color: #fff;
  transition-timing-function: cubic-bezier(.55,.055,.675,.19);
  transition-duration: 75ms;

  &&:before {
    position: absolute;
    content: "";
    top: -7px;
    width: 1.45rem;
    height: 3px;
    transition-timing-function: ease;
    transition-duration: .15s;
    transition-property: transform;
    transition: top 75ms ease .12s, opacity 75ms ease;
    border-radius: 4px;
    background-color: #fff;

    ${p => p.isToggled && css`
      top: 0;
      transition: top 75ms ease,opacity 75ms ease .12s;
      opacity: 0;
    `}
  }

  &&:after {
    position: absolute;
    bottom: -7px;
    content: "";
    width: 1.45rem;
    height: 3px;
    transition-timing-function: ease;
    transition-duration: .15s;
    transition-property: transform;
    border-radius: 4px;
    background-color: #fff;

    ${p => p.isToggled && css`
      bottom: 0;
      transition: bottom 75ms ease,transform 75ms cubic-bezier(.215,.61,.355,1) .12s;
      transform: rotate(-90deg);
    `}
  }

  ${p => p.isToggled && css`
    transition-delay: .12s;
    transition-timing-function: cubic-bezier(.215,.61,.355,1);
    transform: rotate(45deg);
  `}
`

Hamburger.defaultProps = {
  isToggled: false,
  handleChange: false
}

export default Hamburger