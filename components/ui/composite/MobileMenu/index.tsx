import styled from 'styled-components'
import { Container } from '@components/ui/atoms'

const MobileMenu = ({ list }) => {
  return (
    <Box>
      <NavWrapper>
        <Container>
          <MenuLinks>
            {list}
            <li>
              <Phone href={'tel:88007072076'}>8 800 707-20-76</Phone>
            </li>
          </MenuLinks>
        </Container>
      </NavWrapper>
    </Box>
  )
}

const Box = styled('div')`
  position: relative;
  &&:before {
    position: absolute;
    content: '';
    top: 0;
    height: 100vh;
    background: #000;
    z-index: 99;
    width: 100vw;
    margin: 0 auto;
    display: flex;
    opacity: .3;
  }
`

const NavWrapper = styled('div')`
  background-color: #fff;
  width: 100%;
  height: auto;
  position: absolute;
  z-index: 100;
  top: 0;
  padding: 30px 0;
`

const MenuLinks = styled('ul')`
  display: flex;
  justify-content: flex-end;
  align-self: center;
  flex-direction: column;
  height: 100%;

  && > li {
    margin-top: ${p => p.theme.space[2]};
    align-self: start;
  }
`

const Phone = styled('a')`
  color: ${p => p.theme.colors.purple};
  font-size: 18px;
  font-weight: 600;
`

export default MobileMenu