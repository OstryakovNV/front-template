import styled, { css } from 'styled-components'
import { Container } from '@components/ui/atoms'
import { NAVIGATION_MENU } from '@lib/constants'

const Footer = () => {
  return (
    <FooterBox>
      <Container>
      </Container>
    </FooterBox>
  )
}

const FooterBox = styled('footer')`
  background-color: ${p => p.theme.colors.purple};
  height: 400px;
  width: 100%;
  padding: ${p => p.theme.space[5]} 0;
  display: flex;
  align-items: center;
`

export default Footer