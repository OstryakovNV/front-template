export interface ThemeTypes {
  grid: {
    breakpoints: {
      tablet: number;
      desktop: number;
      largeDesktop: number;
    },
    columns: number,
    gutter: number,
    mq: (key: string | number) => string,
    mqMax: (key: string | number) => string,
    mqMinMax: (from: string | number, to: string | number) => string
  }
}

export type ISpaces = 0 | 4 | 8 | 12 | 16 | 20 | 24 | 32 | 48 | 64
export type IFontSizes = 0 | 12 | 14 | 16 | 18 | 20 | 24 | 32 | 40 | 48

