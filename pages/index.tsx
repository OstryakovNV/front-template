import styled, { css } from 'styled-components'
import { Container } from "@components/ui/atoms"
import { Page, Section } from '@components/ui/composite'
import { useState, useEffect, useRef } from 'react'
import { scrollDirectionDetect } from '@utils/common'
import {
  motion,
  useViewportScroll,
  useSpring,
  useTransform
} from "framer-motion";


export default function Home() {
  const [state, setState] = useState(0)
  const ref = useRef(null)


  const [isComplete, setIsComplete] = useState(false);
  const { scrollYProgress } = useViewportScroll();
  const yRange = useTransform(scrollYProgress, [0, 0.9], [0, 1]);
  const pathLength = useSpring(yRange, { stiffness: 400, damping: 90 });
  //console.log('y', yRange)
  useEffect(() => yRange.onChange(v => setState(-v * 1000)), [yRange]);
  //console.log('scrollYProgress', scrollYProgress)

  //const a = scrollDirectionDetect()

  // useEffect(() => {
  //   ref.current.style.transform = `translate(-${scrollYProgress}%, 0%) translate3d(0px, 0px, 0px)`;
  //   //ref.current.addEventListener('mouseDonw')
  // }, [scrollYProgress])

  return (
    <Page>
      <Wrapper>
        <Inner ref={ref} animate={{ x: state }} transition={{ type: "spring", stiffness: 100 }}>
          <Box>1</Box>
          <Box>2</Box>
          <Box>3</Box>
          <Box>4</Box>
          <Box>5</Box>
          <Box>6</Box>
          <Box>7</Box>
          <Box>8</Box>
        </Inner>
      </Wrapper>
    </Page>
  )
}

const Wrapper = styled('div')`
  overflow: hidden;
  position: relative;
  height: 1000px;
  margin: 500px 0;
  width: 100%;
`

const Inner = styled(motion.div)`
  height: 200px;
  display: flex;
  width: max-content;
`

const Box = styled('div')`
  width: 400px;
  height: 200px;
  background: blue;
  border: 1px solid black;
`

// export const getStaticProps = async () => {
//   const data = await getMainPage();

//   return {
//     props: {
//       data
//     }
//   };
// };