// import { Landing, Path } from './types'

const API_URL = process.env.NEXT_PUBIC_STRAPI_API_URL

async function fetchCmsAPI(query: string, { variables }: { variables?: Record<string, any> } = {}) {
  const res = await fetch(API_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      query,
      variables
    })
  });

  const json = await res.json();
  if (json.errors) {
    // eslint-disable-next-line no-console
    console.error("json.errors", json.errors);
    throw new Error('Failed to fetch API');
  }

  return json.data;
}

export async function basicRequest()/*: Promise<Array<Path>> */ {
  const data = await fetchCmsAPI(`
  {
  }
  `)
  return data.landings.data
}