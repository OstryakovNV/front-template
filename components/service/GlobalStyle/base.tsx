import { css } from 'styled-components'

import theme from '@components/context/Theme'


export default css`

  html {
    font-size: ${theme.mobileSize};
    /* overflow-x: hidden; */
  }

  body {
    color: ${theme.colors.black};
    font-family: ${theme.fonts.body};
    background-color: white;
    overflow-x: hidden;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    box-sizing: border-box;
    touch-action: manipulation;
    scroll-behavior: smooth;
    margin: 0;
    padding: 0;
  }

  a,
  a.text:active,
  a.text:hover, 
  a.text {
    text-decoration: none;
    transition: color 0.2s ease;
    color: ${theme.colors.black};
    cursor: pointer,
  }

  /* h1 {
    font-size: ${theme.fontSizes.superLarge};
    color: ${theme.colors.black};
    font-weight: 800;
    line-height: 40px;
  } */
  
  h1, h2 {
    font-size: ${theme.fontSizes.extraLarge};
    font-weight: 700;
    line-height: 32px;
    color: ${theme.colors.black};
  }

  h3 {
    font-size: ${theme.fontSizes.large};
    font-weight: 400;
  }

  h4 {
    font-size: ${theme.fontSizes.small};
    font-weight: 500;
  }

  p {
    line-height: 22px;
    font-size: ${theme.fontSizes.small};
  }

  div {
    line-height: 22px;
    font-size: ${theme.fontSizes.middle};
  }

  span {
    font-size: ${theme.fontSizes.small};
    font-weight: 400;
    color: ${theme.colors.black};
  }

  button {
    font-size: 1rem;
    line-height: 1rem;
    display: inline-flex;
    grid-auto-flow: column;
    position: relative;
    appearance: none;
    transition: all 0.3s ease-out;
    text-decoration: none;
    outline: none;
    user-select: none;
    border: none;
    cursor: pointer;
    background-color: transparent;
    align-items: center;
    text-align: center;
    border-radius: 50px;
    margin: 0;
    padding: .75rem 1rem;
    justify-content: center;
    font-weight: 400;
    width: fit-content;

    &:disabled {
      pointer-events: none;
    }
  }

  input, select {
    outline: 2px solid transparent;
    outline-offset: 2px;
    appearance: none; 
    overflow: hidden;

    &:focus, :active {
      box-shadow: 2px 2px 10px rgb(0 0 0 / 10%);
      z-index: 1;
    }
  }

  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  @media (min-width: ${theme.breakpoints.tablet}px) and (max-width: ${theme.breakpoints.desktop}px) {
    html {
      font-size: ${theme.tabletSize};
    }
  }
  @media(min-width: ${theme.breakpoints.desktop}px) {
    html {
      font-size: ${theme.mainSize};
    }
  }

  @media screen and (-webkit-min-device-pixel-ratio: 0) {
  select:focus, textarea:focus, input:focus {
        font-size: 14px;
      }
  }

  @media (max-width: ${theme.breakpoints.desktop}px) {
    h2 {
      font-size: 1.8rem;
      line-height: 1.8rem;
    }
    h3 {
      font-size: 1.3rem;
    }
  }
` 