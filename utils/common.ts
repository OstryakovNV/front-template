import { useState, useCallback, useEffect, useRef } from "react"
import { getStandartMonth } from 'utils/date'

const getDocumentHeight = (): number => document.documentElement.clientHeight
const getMiddlePosition = (): number => window.pageYOffset - Number(getDocumentHeight()) / 2

const getDimensions = (element: HTMLElement) => {
  const { height } = element.getBoundingClientRect()
  const offsetTop = element.offsetTop
  const offsetBottom = offsetTop + height

  return {
    height,
    offsetTop,
    offsetBottom,
  };
};

/**
 * Хук-переключатель
 * @param {boolean} initialState - первичное значение
 * @returns {void} - [isToggled, toggle]
 */
export const useToggle = (initialState: boolean): [boolean, () => void] => {
  const [isToggled, setIsToggled] = useState(initialState)
  const toggle = useCallback(() => setIsToggled(!isToggled), [isToggled])
  return [isToggled, toggle]
}

/**
 * Формирует окончания числительных
 * @param {number} number - Количество значений
 * @param {Object[]} titles - Список вариаций
 * @returns {string} - Правильный вариант
 */
export function printEnding(number: number, titles: Object[]): string {
  const cases = [2, 0, 1, 1, 1, 2]
  return String(titles[number % 100 > 4 && number % 100 < 20 ? 2 : cases[number % 10 < 5 ? number % 10 : 5]])
}

/**
 * Плавный скролл до нужной позиции
 * @param {HTMLElement} element - ref.current элемент
 * @returns {void} - window скролл
 */
export const smoothScrollTo = (element, yOffset = 50): void => {
  const { offsetTop } = getDimensions(element)
  const offset = offsetTop + yOffset
  window.scrollTo({ top: offset, behavior: 'smooth' })
}

type handleScrollType = {
  name?: string,
  ref: React.RefObject<HTMLElement>
}

/**
 * Позволяет определить ближайший элемент, который пересек вертикальную середину viewport
 * @param {object[]} menuList - массив с обьектами списка меню
 * @returns {string} 
 */
export const setActiveMenu = (menuList: handleScrollType[]) => {
  const [visibleSection, setVisibleSection] = useState(menuList[0].name)
  const handleScroll = () => {

    const middlePosition = Number(getMiddlePosition())

    const selected = menuList.find(({ ref }) => {
      const element = ref.current
      if (element) {
        const { offsetBottom, offsetTop } = getDimensions(element)
        const offset = middlePosition - (offsetTop + offsetBottom) / 2
        return offset > 10 && offset < 150
      }
    });

    if (selected) {
      return setVisibleSection(selected.name)
    }
  };
  useEffect(() => {
    window.addEventListener("scroll", handleScroll)
    return () => {
      window.removeEventListener("scroll", handleScroll)
    };
  }, [visibleSection])
  return visibleSection
}

/**
 * Плавный скролл к указанному ref
 * @param {React.RefObject<T>} ref 
 * @returns {void} 
 */
export const scroll = (ref): void => ref.current.scrollIntoView({
  behavior: 'smooth',
  block: 'center',
})

/**
 * Обсервер
 * @param {React.RefObject<T>} ref 
 * @returns {void} 
 */
export const useOnScreen = (ref) => {

  const [isIntersecting, setIntersecting] = useState(false)

  const observer = new IntersectionObserver(
    ([entry]) => setIntersecting(entry.isIntersecting)
  )

  useEffect(() => {
    observer.observe(ref.current)
    return () => {
      observer.disconnect()
    }
  }, [])

  return isIntersecting
}

export const scrollDirectionDetect = (): number => {
  const [y, setY] = useState(window.scrollY);

  const handleNavigation = useCallback(
    e => {
      const window = e.currentTarget;
      setY(window.scrollY);
    }, [y]
  );

  useEffect(() => {
    setY(window.scrollY);
    window.addEventListener("scroll", handleNavigation);

    return () => {
      window.removeEventListener("scroll", handleNavigation);
    };
  }, [handleNavigation]);

  return Number(y.toFixed(5))
}