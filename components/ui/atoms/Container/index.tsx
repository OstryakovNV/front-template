import styled, { css } from "styled-components";

interface ContainerTypes {
  theme?: any,
  isFluid?: boolean,
}

const Container = styled("div")`
  width: 100%;
  
  ${({ theme }: ContainerTypes) => css`
    padding-right: ${theme.grid.gutter / 2}px;
    padding-left: ${theme.grid.gutter / 2}px;
  `}
  
  margin-right: auto;
  margin-left: auto;
  position: relative;
  
  ${({ isFluid, theme }: ContainerTypes) => !isFluid && css`
    max-width: 480px;
    
    ${Object.keys(theme.grid.breakpoints).map((bpItem) => `
      ${theme.grid.mq(bpItem)} {
        max-width: ${theme.grid.maxContainerWidth[bpItem]}px;
      }
    `)}
  `}
`;

Container.defaultProps = {
  isFluid: false,
}

export default Container;
