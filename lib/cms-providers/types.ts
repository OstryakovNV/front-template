export interface Image {
  attributes: {
    url: string
    blurDataURL: string
  }
}