import styled, { css } from "styled-components";
import { Box } from '@components/ui/atoms';
import { ThemeTypes } from '@components/context/Theme/@types';

interface ColTypes {
  size?: object;
  theme?: ThemeTypes;
  mt?: number,
  mb?: number,
  mr?: number,
  ml?: number,
  mx?: number,
  my?: number,
}

const Col = styled(Box) <ColTypes>`
  position: relative;
  width: 100%;
  /* line-height: 1.83rem; //возможно будет мешать */

  ${p => p.mt && css`
    margin-top: ${p.mt}rem;
  `}
  ${p => p.mb && css`
    margin-bottom: ${p.mb}rem;
  `}
  ${p => p.mr && css`
    margin-right: ${p.mr}rem;
  `}
  ${p => p.ml && css`
    margin-left: ${p.ml}rem;
  `}
  ${p => p.my && css`
    margin-top: ${p.my}rem;
    margin-bottom: ${p.my}rem;
  `}
  ${p => p.mx && css`
    margin-right: ${p.mx}rem;
    margin-left: ${p.mx}rem;
  `}

  // /home/nikita19095/sites/moymir-media-front/.next/server/chunks/466.js:96:83
  ${({ theme }) => css`
    padding-right: ${theme.grid.gutter / 2}px;
    padding-left: ${theme.grid.gutter / 2}px;
  `}
  
  ${({ size, theme }: ColTypes) => size && Object.entries(size).map(([key, value]) => {
  let colValue;
  let colWidth;
  let styles;
  const isAuto = typeof value === 'string' && value === 'auto';
  const isNull = typeof value !== 'string' && typeof value !== 'number' && value === null || value === 0;

  if (isNull) {
    styles = `
        flex-basis: 0;
        flex-grow: 1;
        max-width: 100%;
      `;

    if (key === 'default') {
      return css`
          ${styles}
        `
    }

    return css`
        ${theme.grid.mq(key)} {
          ${styles}
        }
      `
  }

  if (isAuto) {
    colWidth = 'auto';
  } else {
    colValue = 100 / 12 * Number(value);
    colWidth = `${Number.isInteger(colValue) ? colValue : colValue.toFixed(7)}%`;
  }

  styles = `
      flex: 0 0 ${colWidth};
      ${isAuto ? 'width: auto;' : ''}
      max-width: ${isAuto ? '100%' : colWidth};
    `;

  if (key === 'default') {
    return css`
        ${styles}
      `
  }

  return css`
      ${theme.grid.mq(key)} {
        ${styles}
      }
    `
})}
`;

Col.defaultProps = {
  size: {
    default: null
  }
}

export default Col;
