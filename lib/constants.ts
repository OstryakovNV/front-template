export const SITE_URL = ''
export const SITE_ORIGIN = process.env.NEXT_PUBLIC_SITE_ORIGIN || new URL(SITE_URL).origin
export const INSTAGRAM_URL = ''
export const BRAND_NAME = ''
export const SITE_NAME_MULTILINE = ['ACME', 'Conf']
export const SITE_NAME = ''
export const META_TITLE = ''
export const META_DESCRIPTION = ''
export const SITE_DESCRIPTION = ''
export const SITE_KEYWORDS = ''
export const COOKIE = 'user-id'
export const STRAPI_URL = process.env.NEXT_PUBIC_STRAPI_API_URL

// Remove process.env.NEXT_PUBLIC_... below and replace them with
// strings containing your own privacy policy URL and copyright holder name
export const LEGAL_URL = process.env.NEXT_PUBLIC_PRIVACY_POLICY_URL
export const COPYRIGHT_HOLDER = process.env.NEXT_PUBLIC_COPYRIGHT_HOLDER

export const NAVIGATION_MENU = [
  {
    id: 0,
    name: 'главная',
    link: '/'
  },
  {
    id: 1,
    name: 'товары',
    link: '#'
  },
  {
    id: 2,
    name: 'о нас',
    link: '#'
  },
  {
    id: 3,
    name: 'контакты',
    link: '#'
  },
]

export type TicketGenerationState = 'default' | 'loading';
