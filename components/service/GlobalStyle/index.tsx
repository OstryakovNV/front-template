import { createGlobalStyle } from 'styled-components'
import modernNormalize from './modernNormalize'
import myReset from './myReset'

import base from './base'

const GlobalStyle = createGlobalStyle`
  ${modernNormalize}
  ${myReset}
  ${base}
`
export default GlobalStyle
