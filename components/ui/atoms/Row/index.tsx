import styled, { css } from "styled-components";
import { Box } from '@components/ui/atoms';

const Row = styled(Box)`
  display: flex;
  flex-wrap: wrap;
  
  ${({ theme }) => css`
    margin-right: -${(theme.grid.gutter / 2)}px;
    margin-left: -${theme.grid.gutter / 2}px;
  `}
`;

export default Row;