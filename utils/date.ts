const today = () => new Date()
const valueDate = (value: string) => new Date(value)
const isPositive = (value: number) => value > 0

const NAME_OF_MONTH = [
  'Январь',
  'Февраль',
  'Март',
  'Апрель',
  'Май',
  'Июнь',
  'Июль',
  'Август',
  'Сентябрь',
  'Октябрь',
  'Ноябрь',
  'Декабрь'
]

export const setMainDateFormat = (value: string) => {
  return valueDate(value).toLocaleDateString('ru-RU', { month: "numeric", day: "numeric", year: "numeric" })
}

export const getStandartMonth = (date) => valueDate(date).getMonth()

export const determineDate = (value: string) => valueDate(value).valueOf() - today().valueOf()

export const vacantDate = (value: string) => isPositive(determineDate(value))

export const resultDate = (date_start: string, date_end: string) => {
  return `${setMainDateFormat(date_start)} - ${setMainDateFormat(date_end)}`
}

export const getNameOfMonth = (num: number) => NAME_OF_MONTH.find((el, index) => index === num)

export const sortDate = (current: string, next: string): any => valueDate(current).valueOf() - valueDate(next).valueOf()

export const getDay = (date: string): string => ('0' + valueDate(date).getDate().toString()).slice(-2)

export const getMonth = (date: string): string => ('0' + getStandartMonth(date)).toString().slice(-2)

export const getYear = (date: string): number => valueDate(date).getFullYear()