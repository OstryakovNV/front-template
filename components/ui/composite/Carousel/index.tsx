import { forwardRef } from 'react'
import styled from 'styled-components'
import Slick from 'react-slick'
import SlickCss from './styled'

const Carousel = (props, ref) => {
  return (
    <SliderCarousel>
      <SlickCss />
      <Slick ref={ref} {...props} />
    </SliderCarousel>
  )
}

const SliderCarousel = styled('div')`
  background-color: white;
`

export default forwardRef(Carousel)
