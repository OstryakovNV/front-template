import breakpointUtils from 'utils/breakpoints'
import { ISpaces, IFontSizes } from './@types'

const breakpoints = {

  // Medium devices (tablets, 648px and up)
  tablet: 648,

  // Large devices (desktops, 1280px and up)
  desktop: 1280,

  // Extra large devices (large desktops, 1440px and up)
  largeDesktop: 1440,
}


const gridGutter = 30;

const maxContainerWidth = Object.entries(breakpoints).reduce((acc, [key, value]) => {
  acc[key] = value - gridGutter;
  return acc;
}, {});

const colors = {
  //set colors
  black: "#000000"
}

const mainSize = '16px'
const tabletSize = '16px'
const mobileSize = '14px'

const fontSizes = {
  superLarge: '2.5rem',
  extraLarge: '2rem', // 110px
  large: '1.5rem',       // 24px
  middle: '1rem',        // 16px
  small: '.875rem',      // 14px
}

const space = {

  // set spaces


  /* default spaces
  0: '0rem',    // 0px
  1: '.5rem',   // 8px
  2: '1rem',    // 16px
  3: '1.5rem',  // 24px
  4: '2rem',    // 32px
  5: '2.5rem',  // 40px
  6: '3rem',    // 48px
  7: '3.5rem',  // 56px
  8: '4rem',    // 64px
  9: '4.5rem',  // 72px
  10: '5rem',   // 80px
  11: '5.5rem', // 88px
  12: '6rem',   // 96px
  13: '6.5rem', // 104px
  14: '7rem',   // 112px
  15: '7.5rem', // 120px
  */
}

const mainFontFamily = `-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'`
const monoFontFamily = `SFMono-Regular, Consolas, 'Liberation Mono', Menlo, Courier, monospace`

const fonts: Record<string, any> = {
  body: `Montserrat, ${mainFontFamily}`,
  heading: `Montserrat, ${mainFontFamily}`,
  monospace: monoFontFamily,
}

const fontWeights = {
  normal: 400,
  medium: 500,
  semibold: 600,
  bold: 700,
  black: 900,
}


const bpu = breakpointUtils(breakpoints);

const mq = (key: string | number) => `@media screen and ${bpu.getMediaQuery(key)}`;
const mqMax = (key: string | number) => `@media screen and ${bpu.getMediaQuery(key, true)}`;
const mqMinMax = (from: string | number, to: string | number) => `@media screen and ${bpu.getMediaQuery([from, to])}`;

const grid = {
  breakpoints,
  maxContainerWidth,
  gutter: gridGutter,
  columns: 12,
  mq,
  mqMax,
  mqMinMax,
  ...bpu
}

export default {
  grid,
  fonts,
  fontWeights,
  space,
  fontSizes,
  breakpoints,
  mainSize,
  tabletSize,
  mobileSize,
  colors,
};