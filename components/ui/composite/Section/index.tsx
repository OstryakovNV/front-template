import styled, { css } from 'styled-components'
import { Container } from "@components/ui/atoms"
import { forwardRef } from 'react'

interface ComponentProps {
  children: any
  forwardedRef?: any
  space?: number
  colorBackground?: boolean
  ref: any
}

const Section = ({ children, space, colorBackground }: ComponentProps, ref) => {
  return (
    <SectionBox ref={ref} space={space} colorBackground={colorBackground}>
      <Container>
        {children}
      </Container>
    </SectionBox>
  )
}

const SectionBox = styled('section') <ComponentProps>`
  padding: ${p => p.theme.space[p.space]} 0;
  ${(({ colorBackground }) => colorBackground && css`
      background-color: ${p => p.theme.colors.purple};
  `)}
`

export default forwardRef(Section)