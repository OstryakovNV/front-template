import Image from 'next/image'
import { ImageProps } from 'next/image'
import { Image as ImageTypes } from '@lib/cms-providers/types'

type OmitImageProps = Omit<ImageProps, "src">;

interface ComponentProps extends OmitImageProps {
  useplaceholder?: boolean
  image: ImageTypes
}

const ImageNext = ({ image, useplaceholder, ...props }: ComponentProps) => {
  const blurDataURL = image.attributes.blurDataURL || null
  const usePlaceholder = (useplaceholder && !!blurDataURL) && {
    placeholder: 'blur',
    blurDataURL
  }
  return (
    // @ts-ignore
    <Image
      src={image.attributes.url}
      alt={props.alt || 'image'}
      {...usePlaceholder}
      {...props}
    />
  )
}

ImageNext.defaultProps = {
  useplaceholder: false
}

export default ImageNext