import { SSRProvider } from 'react-aria'
import { useEffect, useState, createContext } from 'react'
import type { AppProps } from 'next/app'
import { ThemeProvider } from 'styled-components'
import { useRouter } from 'next/router'
import Head from 'next/head'
import Theme from '@components/context/Theme'
import GlobalStyle from '@components/service/GlobalStyle'
import { META_TITLE, SITE_URL, SITE_KEYWORDS, SITE_DESCRIPTION } from '@lib/constants';

// Store client object in context
export const ClientContext = createContext({
  screenName: null
});

const App = ({ Component, pageProps }: AppProps) => {
  const router = useRouter()
  const url = `${SITE_URL}${router.asPath}`

  const [screenName, setScreenName] = useState(null)
  const resizeHandler = () => setScreenName(Theme.grid.getCurrentMediaQueryName())

  const appHeight = () => {
    const doc = document.documentElement
    doc.style.setProperty('--app-set-height', `${window.innerHeight}px`)
  }

  // Вешаем слушатель ресайза окна
  useEffect(() => {
    appHeight()
    resizeHandler()
    window.addEventListener("resize", resizeHandler)

    return () => window.removeEventListener("resize", resizeHandler)
  }, [])

  return (
    <ThemeProvider theme={Theme}>
      <Head>
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=no;user-scalable=0;" />
        <title>{pageProps?.data?.text?.title ? `${pageProps?.data?.text?.title} - ${pageProps?.data?.text?.label}` : META_TITLE}</title>
        <meta property="og:title" content={pageProps?.data?.text?.title ? `${pageProps?.data?.text?.title} - ${pageProps?.data?.text?.label}` : META_TITLE} />
        <meta property="og:url" content={url} />
        <meta name="description" content={pageProps?.data?.text?.description || SITE_DESCRIPTION} />
        <meta property="og:description" content={pageProps?.data?.text?.description || SITE_DESCRIPTION} />
        <meta property="og:image" content={pageProps?.data?.background?.data?.attributes?.url || `${SITE_URL}/og-image.png`} />
        <meta property="og:type" content="website" />
        <meta name="keywords" content={pageProps?.data?.text?.keywords || SITE_KEYWORDS} />
      </Head>
      <SSRProvider>
        <ClientContext.Provider value={{ screenName }}>
          <GlobalStyle />
          {screenName && (
            <Component {...pageProps} />
          )}
        </ClientContext.Provider>
      </SSRProvider>
    </ThemeProvider>
  );
}

export default App;