import styled, { css } from 'styled-components'
import React, { forwardRef } from 'react'
import { Carousel } from '@components/ui/composite'


interface CarouselTypes {
  settings: any,
  children: any,
  overflow?: boolean,
  bg?: string,
  mr?: number,
  transition?: boolean,
}

const ContentCarousel =
  // @ts-ignore
  ({ settings, children, afterChangeItem, ...props }: CarouselTypes, ref) => {
    const initialSettings = {
      lazyLoad: 'progressive',
      speed: 250,
      dots: false,
      infinite: true,
      swipe: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      mobileFirst: true,
      appendDots: dots => <Dots> {dots} </Dots>,
      customPaging: i => <Dot />,
    }

    return (
      <Carousel {...initialSettings} {...settings} {...props} ref={ref}>
        {children}
      </Carousel>
    )
  }

const Dot = styled('div')`
  border-radius: circle;
  cursor: pointer;
  width: 15px;
  height: 15px;
  margin: 0 4px;
  background-color: transparent;
  box-shadow: inset 0 0 0 2px #5a0296;

  ${p =>
    p.theme.mobile &&
    css`
      width: 12px;
      height: 12px;
    `}
`

const Dots = styled('ul')`
  padding: 0;
  margin: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: circle;

  .slick-active ${Dot} {
    background-color: #5a0296;
    box-shadow: none;
  }
`


export default forwardRef(ContentCarousel)