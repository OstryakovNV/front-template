import styled from "styled-components";

const Box = styled.div`
  &::after,
  &::before {
    box-sizing: border-box;
  }
`;

export default Box;