const withPlugins = require('next-compose-plugins')
const optimizedImages = require('next-optimized-images')

const nextConfig = {
  images: {
    domains: ['res.cloudinary.com']
  },
  compiler: {
    // ssr and displayName are configured by default
    styledComponents: true,
  },
  swcMinify: true,
}

const plugins = [
  [
    optimizedImages
  ]
]

module.exports = withPlugins([...plugins], nextConfig)
