import React from 'react'
import { Seo } from '@components/ui/atoms'
import { Image as ImageType } from '@lib/cms-providers/types'
import { Header, Footer } from '@components/ui/templates'

interface ComponentProps {
  children: any,
  meta?: {
    mainImage?: {
      data: ImageType
    }
    title?: string
    description?: string
  },
}

const Page = ({ children, meta }: ComponentProps) => {
  return (
    <>
      <Seo meta={meta} />
      <Header />
      {children}
      <Footer />
    </>
  )
}

export default Page