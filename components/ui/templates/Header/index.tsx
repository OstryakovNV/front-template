import styled, { css } from 'styled-components'
import { Container, Row, Col, Hamburger } from "@components/ui/atoms"
import Link from "next/link"
import { NAVIGATION_MENU } from '@lib/constants'
import { useContext, useEffect, useState, useMemo } from 'react'
import { ClientContext } from "pages/_app"

const Header = (props) => {
  const { screenName } = useContext(ClientContext)
  const [isMenuVisible, setIsMenuVisible] = useState(false)
  const isMobileDevices = screenName === 'default' || screenName === 'tablet'

  useEffect(() => {
    if (isMenuVisible) {
      document.body.style.overflow = "hidden"
    } else {
      document.body.style.overflow = "auto"
    }
  });

  const NAV_LIST = useMemo(() => {
    return (
      NAVIGATION_MENU.map((item) => (
        <li key={item.id}><MenuLink href={item.link}>{item.name}</MenuLink></li>
      ))
    )
  }, [NAVIGATION_MENU])

  return (
    <HeaderBox>
      <Container>
        <_Row>
          <Col size={{ default: 6 }}>
            <Link href='/'>
              <a>
                {/*logo */}
              </a>
            </Link>
          </Col>
          <Col size={{ default: 6 }}>
            {isMobileDevices ?
              <Hamburger isToggled={isMenuVisible} handleChange={() => setIsMenuVisible(prev => !prev)} />
              :
              <MenuLinks>
                {NAV_LIST}
              </MenuLinks>
            }
          </Col>
        </_Row>
      </Container>
    </HeaderBox>
  )
}

const HeaderBox = styled('header')`
 margin: ${p => p.theme.space[2]} 0;
 ${({ theme }) => css`
    ${theme.grid.mq('desktop')} {
      margin: ${p => p.theme.space[4]} 0;
    }
  `}
`

const _Row = styled(Row)`
  align-items: center;
`

const MenuLinks = styled('ul')`
  display: flex;
  justify-content: flex-end;
  align-self: center;
  height: 100%;

  && > li {
    margin-right: ${p => p.theme.space[2]};
    align-self: center;
  }
`

const MenuLink = styled('a')`
  color: ${p => p.theme.colors.black};
  font-size: 18px;
`

export default Header