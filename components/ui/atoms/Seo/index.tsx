import { SITE_NAME, SITE_URL, SITE_KEYWORDS } from '@lib/constants'
import { Image as ImageType } from '@lib/cms-providers/types'

type Meta = {
  title?: string | null
  description?: string | null
  mainImage?: {
    data: ImageType
  }
  url?: string | null
  keywords?: string | null
};

type Props = {
  meta: Meta;
};

export default function Seo({ meta }: Props) {
  const image = meta?.mainImage?.data.attributes.url || 'icon-512x512.png';
  const title = meta?.title || SITE_NAME;
  // const url = meta.url || `${SITE_URL}${router.asPath}`;
  const description = meta?.description || SITE_NAME;
  const keywords = meta?.keywords || SITE_KEYWORDS;

  return (
    <>
      <title>{title}</title>
      <meta property="og:title" content={title} />
      {/* <meta property="og:url" content={url} /> */}
      <meta name="description" content={description} />
      <meta property="og:description" content={description} />
      <meta name="keywords" content={keywords} />
      {image && (
        <meta
          property="og:image"
          content={image.startsWith('https://') ? image : `${SITE_URL}/${image}`}
        />
      )}
    </>
  );
}
